import React from "react";
import GoogleLogin from "react-google-login";
import axios from "axios";
import { Button } from "@material-ui/core";
import logoGoogle from "../../assets/logoGoogle.svg";

function Google({ informParent = (f) => f }) {
  const responseGoogle = (response) => {
    console.log(response.tokenId);
    axios({
      method: "POST",
      url: `${process.env.REACT_APP_API_URL}/google-login`,
      data: { idToken: response.tokenId },
    })
      .then((response) => {
        console.log("GOOGLE SIGNIN SUCCESS", response);
        // inform parent component
        informParent(response);
      })
      .catch((error) => {
        console.log("GOOGLE SIGNIN ERROR", error.response);
      });
  };
  return (
    <div style={{ wpadding: "10px" }}>
      <GoogleLogin
        clientId={`${process.env.REACT_APP_GOOGLE_CLIENT_ID}`}
        onSuccess={responseGoogle}
        onFailure={responseGoogle}
        render={(renderProps) => (
          <Button
            variant="contained"
            onClick={renderProps.onClick}
            disabled={renderProps.disabled}
            style={{
              backgroundColor: "#288BEE",
              color: "#fff",
              fontWeight: "bold",
              fontFamily: "Roboto",
              textTransform: "inherit",
            }}
          >
            <span style={{ marginRight: "10px" }}>
              <img src={logoGoogle} height="30px" alt="Logo Google" />
            </span>
            <div> Ingresa con Google</div>
          </Button>
        )}
        cookiePolicy={"single_host_origin"}
      />
    </div>
  );
}

export default Google;

import React from "react";
import FacebookLogin from "react-facebook-login/dist/facebook-login-render-props";
import { Button } from "@material-ui/core";
import axios from "axios";

function Facebook({ informParent = (f) => f }) {
  const responseFacebook = (response) => {
    console.log(response);
    axios({
      method: "POST",
      url: `${process.env.REACT_APP_API_URL}/facebook-login`,
      data: { userID: response.userID, accessToken: response.accessToken },
    })
      .then((response) => {
        console.log("FACEBOOK SIGNIN SUCCESS", response);
        // inform parent component
        informParent(response);
      })
      .catch((error) => {
        console.log("FACEBOOK SIGNIN ERROR", error.response);
      });
  };
  return (
    <div>
      <FacebookLogin
        appId={`${process.env.REACT_APP_FACEBOOK_APP_ID}`}
        autoLoad={false}
        callback={responseFacebook}
        render={(renderProps) => (
          <Button
            onClick={renderProps.onClick}
            variant="contained"
            style={{ backgroundColor: "#386BAE", color: "#fff" }}
          >
            <span style={{ marginRight: "10px" }}>
              <i className="fab fa-facebook" />
            </span>
            <div>Ingresa con Facebook</div>
          </Button>
        )}
      />
    </div>
  );
}

export default Facebook;

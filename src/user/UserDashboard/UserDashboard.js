import React, { useState, useEffect } from "react";
import Layout from "../../components/Layout";
import { isAuthenticated } from "../../auth";
import { makeStyles } from "@material-ui/core/styles";
import { Link } from "react-router-dom";
import { getPurchaseHistory } from "../apiUser";
import {
  Button,
  ButtonGroup,
  Card,
  CardContent,
  Typography,
  Box,
} from "@material-ui/core";
import moment from "moment";

const useStyles = makeStyles({
  root: {
    minWidth: 275,
    borderBottom: "5px solid #366797",
    marginBottom: "20px",
  },
});

function UserDashboard() {
  const [history, setHistory] = useState([]);
  const classes = useStyles();
  const {
    user: { _id, email, name, role },
  } = isAuthenticated();
  const token = isAuthenticated().token;

  const init = (userId, token) => {
    getPurchaseHistory(userId, token).then((data) => {
      if (data.error) {
        console.log(data.error);
      } else {
        setHistory(data);
      }
    });
  };

  useEffect(() => {
    init(_id, token);
  }, []);

  const userLinks = () => {
    return (
      <ButtonGroup orientation="vertical" color="primary" variant="contained">
        <Button component={Link} to="/cart">
          Mi carrito
        </Button>
        <Button component={Link} to={`/profile/${_id}`}>
          Editar Perfil
        </Button>
      </ButtonGroup>
    );
  };

  const userInfo = () => {
    return (
      <Card className={classes.root}>
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            color: "#fff",
            backgroundColor: "#366797",
          }}
        >
          <Typography variant="h5" component="h2">
            Datos Usuario
          </Typography>
        </div>
        <CardContent>
          <Typography>Nombre: {name}</Typography>
          <Typography>Email: {email}</Typography>
          <Typography variant="body2" component="p">
            Role: {role === 1 ? "Admin" : "Registered User"}
          </Typography>
        </CardContent>
      </Card>
    );
  };

  const purchaseHistory = (history) => {
    return (
      <Card className={classes.root}>
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            color: "#fff",
            backgroundColor: "#366797",
          }}
        >
          <Typography variant="h5" component="h2">
            Historial de Compras
          </Typography>
        </div>
        <CardContent>
          {history.map((h, i) => {
            return (
              <div>
                <hr />
                {h.products.map((p, i) => {
                  return (
                    <div key={i}>
                      <Typography>
                        <strong>Producto:</strong> {p.name}
                      </Typography>
                      <Typography>
                        <strong>Precio:</strong> ${p.price}
                      </Typography>
                      <Typography>
                        <strong>Fecha de compra: </strong>
                        {moment(p.createdAt).fromNow()}
                      </Typography>
                    </div>
                  );
                })}
              </div>
            );
          })}
        </CardContent>
      </Card>
    );
  };

  // };

  return (
    <Layout>
      <div style={{ display: "flex", justifyConten: "center" }}>
        <div style={{ padding: "0 30px" }}>{userLinks()}</div>
        <div style={{ width: "70%" }}>
          {userInfo()}
          {purchaseHistory(history)}
        </div>
      </div>
    </Layout>
  );
}

export default UserDashboard;

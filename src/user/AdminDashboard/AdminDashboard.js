import React from "react";
import Layout from "../../components/Layout";
import { isAuthenticated } from "../../auth";
import { Link } from "react-router-dom";
import { makeStyles } from "@material-ui/core/styles";
import {
  Button,
  ButtonGroup,
  Card,
  CardContent,
  Typography,
  Box,
} from "@material-ui/core";

const useStyles = makeStyles({
  root: {
    minWidth: 275,
    borderBottom: "5px solid #366797",
  },
});

const AdminDashboard = () => {
  const classes = useStyles();
  const {
    user: { _id, name, email, role },
  } = isAuthenticated();

  const adminLinks = () => {
    return (
      <ButtonGroup orientation="vertical" color="primary" variant="contained">
        <Button component={Link} to="/create/category">
          Crear Categoria
        </Button>
        <Button component={Link} to="/create/product">
          Crear Producto
        </Button>
        <Button component={Link} to="/admin/orders">
          Pedidos
        </Button>
        <Button component={Link} to="/admin/products">
          Administrar Productos
        </Button>
        <Button component={Link} to="/user/dashboard">
          Perfil
        </Button>
      </ButtonGroup>
    );
  };

  const adminInfo = () => {
    return (
      <Card className={classes.root}>
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            color: "#fff",
            backgroundColor: "#366797",
          }}
        >
          <Typography variant="h5" component="h2">
            Datos Usuario
          </Typography>
        </div>
        <CardContent>
          <Typography>Nombre: {name}</Typography>
          <Typography>Email: {email}</Typography>
          <Typography variant="body2" component="p">
            Role: {role === 1 ? "Admin" : "Registered User"}
          </Typography>
        </CardContent>
      </Card>
    );
  };

  return (
    <Layout>
      <Box display="flex" alignItems="center" justifyContent="center">
        {adminLinks()}
        <div style={{ width: "500px", paddingLeft: "30px" }}>{adminInfo()}</div>
      </Box>
    </Layout>
  );
};

export default AdminDashboard;

import React, { useState, useEffect } from "react";
import {
  getProducts,
  getBraintreeClientToken,
  processPayment,
  createOrder,
} from "../apiComponentes";
import { emptyCart } from "../cartHelpers";
import { isAuthenticated } from "../../auth";
import { Link } from "react-router-dom";
import DropIn from "braintree-web-drop-in-react";
import {
  Card,
  TextField,
  Button,
  FormControl,
  Typography,
  LinearProgress,
} from "@material-ui/core";
import Alert from "@material-ui/lab/Alert";

function Checkout({ products, setRun = (f) => f, run = undefined }) {
  const [data, setData] = useState({
    loading: false,
    success: false,
    clientToken: null,
    error: "",
    instance: {},
    address: "",
  });

  const userId = isAuthenticated() && isAuthenticated().user._id;
  const token = isAuthenticated() && isAuthenticated().token;

  const getToken = (userId, token) => {
    getBraintreeClientToken(userId, token).then((data) => {
      if (data.error) {
        console.log(data.error);
        setData({ ...data, error: data.error });
      } else {
        console.log(data);
        setData({ clientToken: data.clientToken });
      }
    });
  };

  useEffect(() => {
    getToken(userId, token);
  }, []);

  const handleAddress = (event) => {
    setData({ ...data, address: event.target.value });
  };

  const getTotal = () => {
    return products.reduce((currentValue, nextValue) => {
      return currentValue + nextValue.count * nextValue.price;
    }, 0);
  };

  const showCheckout = () => {
    return isAuthenticated() ? (
      <div>{showDropIn()}</div>
    ) : (
      <div
        style={{ display: "flex", justifyContent: "center", padding: "20px" }}
      >
        <Button
          component={Link}
          to="/signin"
          variant="outlined"
          color="primary"
        >
          logueate para comprar
        </Button>
      </div>
    );
  };

  let deliveryAddress = data.address;

  const buy = () => {
    setData({ loading: true });

    let nonce;
    let getNonce = data.instance
      .requestPaymentMethod()
      .then((data) => {
        nonce = data.nonce;

        const paymentData = {
          paymentMethodNonce: nonce,
          amount: getTotal(products),
        };

        processPayment(userId, token, paymentData)
          .then((response) => {
            console.log(response);

            const createOrderData = {
              products: products,
              transaction_id: response.transaction.id,
              amount: response.transaction.amount,
              address: deliveryAddress,
            };

            createOrder(userId, token, createOrderData)
              .then((response) => {
                emptyCart(() => {
                  setRun(!run);

                  setData({
                    loading: false,
                    success: true,
                  });
                });
              })
              .catch((error) => {
                console.log(error);
                setData({ loading: false });
              });
          })
          .catch((error) => {
            console.log(error);
            setData({ loading: false });
          });
      })
      .catch((error) => {
        setData({ ...data, error: error.message });
      });
  };

  const showDropIn = () => (
    <div onBlur={() => setData({ ...data, error: "" })}>
      {data.clientToken !== null && products.length > 0 ? (
        <div style={{ padding: "10px" }}>
          <FormControl style={{ width: "100%" }}>
            <Typography>Dirección Delivery:</Typography>
            <TextField
              fullWidth
              onChange={handleAddress}
              variant="outlined"
              value={data.address}
              placeholder="Escribí tu dirección..."
            />
          </FormControl>

          <DropIn
            options={{
              authorization: data.clientToken,
              paypal: {
                flow: "vault",
              },
            }}
            onInstance={(instance) => (data.instance = instance)}
          />
          <div
            style={{
              display: "flex",
              justifyContent: "center",
            }}
          >
            <Button
              variant="contained"
              onClick={buy}
              style={{
                backgroundColor: "#00c853",
                color: "#fff",
                width: "100%",
              }}
            >
              Pagar
            </Button>
          </div>
        </div>
      ) : null}
    </div>
  );

  const showError = (error) => (
    <Alert
      variant="filled"
      severity="error"
      style={{ display: error ? "" : "none" }}
    >
      {error}
    </Alert>
  );

  const showSuccess = (success) => (
    <div style={{ padding: "20px" }}>
      <Alert
        variant="filled"
        severity="success"
        style={{ display: success ? "" : "none" }}
      >
        <Typography>Gracias por Elegirnos!</Typography>
      </Alert>
    </div>
  );

  const showLoading = (loading) => loading && <LinearProgress />;

  return (
    <Card style={{ borderBottom: "5px solid #366797", width: "500px" }}>
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          color: "#fff",
          backgroundColor: "#366797",
        }}
      >
        <Typography component="h4" variant="h4">
          Realizar Pago
        </Typography>
      </div>
      <div
        style={{ display: "flex", justifyContent: "center", padding: "10px" }}
      >
        <Typography variant="h4">Total a Pagar: ${getTotal()}</Typography>
      </div>

      {showLoading(data.loading)}
      {showSuccess(data.success)}
      {showError(data.error)}
      {showCheckout()}
    </Card>
  );
}

export default Checkout;

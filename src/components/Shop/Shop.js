import React, { useState, useEffect } from "react";
import Layout from "../Layout";
import ProductCard from "../ProductCard";
import { getCategories, getFilteredProducts } from "../apiComponentes";
import { Button } from "@material-ui/core";
import Search from "../Search";
import VisibilityIcon from "@material-ui/icons/Visibility";
import "./Shop.css";

function Shop() {
  const [myFilters, setMyFilters] = useState({
    filters: { category: [], price: [] },
  });
  const [categories, setCategories] = useState([]);
  const [error, setError] = useState(false);
  const [limit, setLimit] = useState(3);
  const [skip, setSkip] = useState(0);
  const [size, setSize] = useState(0);
  const [filteredResults, setFilteredResults] = useState([]);

  const init = () => {
    getCategories().then((data) => {
      if (data.error) {
        setError(data.error);
      } else {
        setCategories(data);
      }
    });
  };

  const loadFilteredResults = (newFilters) => {
    getFilteredProducts(skip, limit, newFilters).then((data) => {
      if (data.error) {
        setError(data.error);
      } else {
        setFilteredResults(data.data);
        setSize(data.size);
        setSkip(0);
      }
    });
  };

  const loadMore = () => {
    let toSkip = skip + limit;
    getFilteredProducts(toSkip, limit, myFilters.filters).then((data) => {
      if (data.error) {
        setError(data.error);
      } else {
        setFilteredResults([...filteredResults, ...data.data]);
        setSize(data.size);
        setSkip(toSkip);
      }
    });
  };

  const loadMoreButton = () => {
    return (
      size > 0 &&
      size >= limit && (
        <Button
          variant="contained"
          color="primary"
          onClick={loadMore}
          startIcon={<VisibilityIcon />}
        >
          Ver más
        </Button>
      )
    );
  };

  useEffect(() => {
    init();
    loadFilteredResults(skip, limit, myFilters.filters);
  }, []);

  return (
    <Layout>
      <>
        <h2 style={{ padding: "20px" }}>Productos</h2>
        <Search />
        <div>
          <div className="grid-container">
            {filteredResults.map((product, i) => (
              <div className="grid-item" key={i}>
                <ProductCard product={product} />
              </div>
            ))}
          </div>
        </div>
        <div
          style={{
            width: "100%",
            display: "flex",
            justifyContent: "center",
            padding: "20px",
          }}
        >
          {loadMoreButton()}
        </div>
      </>
    </Layout>
  );
}

export default Shop;

import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Layout from "../Layout";
import { getProducts } from "../apiComponentes";
import ProductCard from "../ProductCard";
import { Grid, Box } from "@material-ui/core";
import fondoPan from "../../assets/images/fondoPan.jpg";

function Home() {
  const [productsBySell, setProductsBySell] = useState([]);
  const [productsByArrival, setProductsByArrival] = useState([]);
  const [error, setError] = useState(false);

  const loadProductsBySell = () => {
    getProducts("sold").then((data) => {
      if (data.error) {
        setError(data.error);
      } else {
        setProductsBySell(data);
      }
    });
  };

  const loadProductsByArrival = () => {
    getProducts("createdAt").then((data) => {
      console.log(data);
      if (data.error) {
        setError(data.error);
      } else {
        setProductsByArrival(data);
      }
    });
  };

  useEffect(() => {
    loadProductsByArrival();
    loadProductsBySell();
  }, []);

  return (
    <Layout>
      <Grid container>
        <Grid item xs={12}>
          <img
            src={fondoPan}
            alt="fondo Pan"
            width="100%"
            style={{ marginTop: "-200px" }}
          />
        </Grid>
        <Grid item xs={12} style={{ margin: "0 20px" }}>
          <h1>Nosotros</h1>
          <div style={{ fontFamily: "Arial" }}>
            Nos encanta hornear, a través de nuestros panes nos conectamos con
            nuestra comunidad de nuestro horno a sus mesas. Todos los
            ingredientes que utilizamos son de la mejor calidad y ecológicos
            para asegurarnos de que nuestros productos se mantengan frescos y
            tengan el sabor que nos distingue. Ofrecemos gran variedad de panes
            artesanales y de masa madre.
          </div>
        </Grid>

        <Grid item xs={12} style={{ margin: "30px 0" }}>
          <h2 style={{ margin: "10px 20px" }}>Recien Horneados</h2>

          <div className="grid-container">
            {productsByArrival.map((product, i) => (
              <div key={i} className="grid-item">
                <ProductCard product={product} />
              </div>
            ))}
          </div>
        </Grid>

        <Grid item xs={12}>
          <h2 style={{ margin: "10px 20px" }}>Se venden como pan caliente!</h2>
          <div className="grid-container">
            {productsBySell.map((product, i) => (
              <div key={i} className="grid-item">
                <ProductCard product={product} />
              </div>
            ))}
          </div>
        </Grid>
      </Grid>
    </Layout>
  );
}

export default Home;

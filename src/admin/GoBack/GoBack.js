import React from "react";
import { Button } from "@material-ui/core";
import { Link } from "react-router-dom";
import KeyboardArrowLeftIcon from "@material-ui/icons/KeyboardArrowLeft";

const goBack = () => (
  <div>
    <Button
      component={Link}
      startIcon={<KeyboardArrowLeftIcon />}
      to="/admin/dashboard"
      variant="contained"
      color="secondary"
    >
      VOLVER
    </Button>
  </div>
);

export default goBack;

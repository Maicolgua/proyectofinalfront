import React, { useState } from "react";
import Layout from "../../components/Layout";
import { isAuthenticated } from "../../auth";
import { createCategory } from "../apiAdmin";
import {
  TextField,
  Button,
  Card,
  CardContent,
  CardActions,
  Typography,
  FormControl,
} from "@material-ui/core";
import AddCircleOutlineIcon from "@material-ui/icons/AddCircleOutline";
import GoBack from "../GoBack";
import Alert from "@material-ui/lab/Alert";

function AddCategory() {
  const [name, setName] = useState("");
  const [error, setError] = useState(false);
  const [success, setSuccess] = useState(false);

  const { user, token } = isAuthenticated();

  const handleChange = (e) => {
    setError("");
    setName(e.target.value);
  };

  const clickSubmit = (e) => {
    e.preventDefault();
    setError("");
    setSuccess(false);

    createCategory(user._id, token, { name }).then((data) => {
      if (data.error) {
        setError(true);
      } else {
        setError("");
        setSuccess(true);
      }
    });
  };

  const newCategoryForm = () => (
    <Card
      style={{
        borderBottom: "5px solid #366797",
        margin: "20px 0",
      }}
    >
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          color: "#fff",
          backgroundColor: "#366797",
        }}
      >
        <Typography component="h4" variant="h4">
          Ingresar Categoria
        </Typography>
      </div>
      <CardContent>
        <FormControl style={{ width: "100%" }}>
          <TextField
            required
            onChange={handleChange}
            value={name}
            label="Nombre"
          />
        </FormControl>
      </CardContent>
      <CardActions
        style={{
          display: "flex",
          alignItems: "center",
          justifyContent: "space-between",
        }}
      >
        <Button
          variant="contained"
          startIcon={<AddCircleOutlineIcon />}
          color="primary"
          onClick={clickSubmit}
        >
          Crear
        </Button>
        <GoBack />
      </CardActions>
    </Card>
  );

  const showSuccess = () => {
    if (success) {
      return (
        <Alert variant="filled" severity="success">
          La categoria {name} fue creada
        </Alert>
      );
    }
  };

  const showError = () => {
    if (error) {
      return (
        <Alert variant="filled" severity="error">
          Ya existe esa categoria. Por favor, ingresar una nueva.
        </Alert>
      );
    }
  };

  return (
    <Layout>
      <div style={{ display: "flex", justifyContent: "center" }}>
        <div style={{ width: "70%" }}>
          {showSuccess()}
          {showError()}
          {newCategoryForm()}
        </div>
      </div>
    </Layout>
  );
}

export default AddCategory;

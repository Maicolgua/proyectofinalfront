

## Proyecto Final Master Desarrollo Web Fullstackt



El proyecto es un sitio web de ecommerce de productos panificados. Donde el usuario para comprar un producto tiene que loguearse. <br />
Contiene una parte autoadministrable, donde el administrador puede crear las categorias y productos. También puede ver los pedidos y editar o borrar los productos.


### `Variabes de entorno`

- REACT_APP_API_URL
- REACT_APP_GOOGLE_CLIENT_ID
- REACT_APP_FACEBOOK_ID


